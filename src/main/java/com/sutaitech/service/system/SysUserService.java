package com.sutaitech.service.system;

import java.util.List;

import com.sutaitech.entity.SysUser;
import com.sutaitech.utils.PageInfo;
import com.sutaitech.utils.ResultVo;


public interface SysUserService {
	
	/**
	 * 查询所有
	 * @return
	 */
	public List<SysUser> findAll();
	
	/**
	 * 分页查询
	 * @param pageInfo
	 * @return
	 */
	public PageInfo<SysUser> findAllByPage(PageInfo<SysUser> pageInfo);
	
	/**
	 * 添加系统用户
	 * @param entity
	 * @return
	 */
	public ResultVo addSysUser(SysUser entity);

	/**
	 * 查询系统用户
	 * @param id
	 * @return
	 */
	public SysUser findById(int id);

	/**
	 * 更新系统用户
	 * @param entity
	 * @return
	 */
	public ResultVo updateSysUser(SysUser entity);

    /**
     * 删除系统用户
     * @param entity
     * @return
     */
	public ResultVo deleteSysUser(int id);
}
